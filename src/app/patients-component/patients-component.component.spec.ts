import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { PatientsComponent } from './patients-component.component';
import { PatientsTableComponent } from './../patients-table/patients-table.component';
import { PatientService } from './../patients/patient.service';
import { ActivityService } from './../activities/activities.service';

describe('PatientsComponent', () => {
  let component: PatientsComponent;
  let fixture: ComponentFixture<PatientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsComponent, PatientsTableComponent ],
      imports: [FormsModule, HttpModule],
      providers: [PatientService, ActivityService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    inject([PatientsTableComponent],
      (table) => {
        expect(component).toBeTruthy();
      });
    });

  it('should calculate activities', () => {
    component.
  });
});
