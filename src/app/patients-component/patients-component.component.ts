import { Component, OnInit } from '@angular/core';
import { PatientService } from '../patients/patient.service';
import { Patient } from '../patients/patient/patient.model';
import { Activity } from '../activities/activity.model';
import { ActivityService } from '../activities/activities.service';

@Component({
  selector: 'app-patients-component',
  templateUrl: './patients-component.component.html',
  styleUrls: ['./patients-component.component.scss']
})
export class PatientsComponent implements OnInit {

  patients: Patient[];
  activitiesModel: Activity[];

  constructor(public service: PatientService, public activitiesServ: ActivityService) {
    activitiesServ.getActivity().subscribe(activities => {
      this.activitiesModel = activities;
    });
    this.patients = [];
    this.service.getPatients().subscribe(patients => {
      this.patients = patients;
      this.patients.forEach(patient => {
        this.service.getPatientsDetail(patient.id).subscribe(activities => {
              patient.activities = [];
              patient.activities = (activities);
              this.calculateActivitiesLevel(patient);
          });
      });
    }, (error) => console.log(error));
  }

  ngOnInit() {
  }

  checkDetail = (event) => {
    this.service.getPatientsDetail(event).subscribe(activities => {
      this.patients.forEach(patient => {
        if (patient.id === event) {
          patient.activities = [];
          patient.activities = (activities);
        }
      });
    });
  }

  getPatient = (id) => {
    this.patients.filter(patient => patient.id = id);
  }

  calculateActivitiesLevel = (patient: Patient) => {
    let vigorous = 0;
    let moderate = 0;
    [vigorous, moderate] = this.calculateActivities(patient, vigorous, moderate);
    let level;
      if (patient.activities && vigorous >= 75 || moderate >= 150 || (vigorous >= 25 && moderate >= 100 )) {
        level = 'ok';
      } else {
        level = 'bad';
      }
    return patient.activityLevel = level;
  }

  private calculateActivities = (patient, vigorous, moderate) => {
      patient.activities.forEach(activity => {
        let intensity = this.getType(activity.activity);
        if (intensity === "vigorous") {
          vigorous += activity.minutes;
          console.log(vigorous);
        }
        if (intensity === "moderate") {
          moderate += activity.minutes;
          console.log(moderate);
        }
      });
      return [vigorous, moderate];
  }

  getType = (activity) => {
    for (let act of this.activitiesModel) {
      if (act.activity === activity ) {
        return act.intensity;
      }
    }

    // this.activitiesModel.forEach(act => {
    //   if (activity === act.name) {
    //     return act.intensity;
    //   }
    //   return 'none';
    // });
  }

}
