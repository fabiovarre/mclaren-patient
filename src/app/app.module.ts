import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { PatientsComponent } from './patients-component/patients-component.component';
import { PatientService } from '../app/patients/patient.service';
import { PatientsTableComponent } from './patients-table/patients-table.component';
import { ActivityService } from './activities/activities.service';

const services = [PatientService, ActivityService];

@NgModule({
  declarations: [
    AppComponent,
    PatientsComponent,
    PatientsTableComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    ...services
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
