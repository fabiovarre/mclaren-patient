import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { PatientsTableComponent } from './patients-table.component';
import { Patient } from './../patients/patient/patient.model';

describe('PatientsTableComponent', () => {
  let component: PatientsTableComponent;
  let fixture: ComponentFixture<PatientsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsTableComponent ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate patient status', () => {
    let patient = new Patient({showDetail: false});
    component.showDetail(patient);
    expect(patient.showDetail).toBe(true);
    component.showDetail(patient);
    expect(patient.showDetail).toBe(false);
  });

});
