import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Patient } from '../patients/patient/patient.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-patients-table',
  templateUrl: './patients-table.component.html',
  styleUrls: ['./patients-table.component.scss']
})
export class PatientsTableComponent implements OnInit {

  birthSort: boolean = false;
  heightSort: boolean = false;
  weightSort: boolean = false;
  bmiSort: boolean = false;
  searchByName: string;
  filterByType: string;
  @Input() patients: Patient[];
  @Output() checkPatientDetail: EventEmitter<number> = new EventEmitter<number>();
  constructor() {
    this.searchByName = '';
    this.filterByType = 'all';
  }

  ngOnInit() {
  }

  showDetail = (patient) => {
    patient.showDetail = !patient.showDetail;

    /* in case of lazy load */
    // if (!patient.activities || patient.activities.length < 1) {
    //   this.checkPatientDetail.emit(patient.id);
    // }
  }

  orderByDate = () => {
    if (this.birthSort) {
      this.patients = this.sortBy('birthDate');
    } else {
      this.patients = this.sortBy('birthDate').reverse();
    }
    this.birthSort = !this.birthSort;
  }

  orderByWeight = () => {
    if (this.weightSort) {
      this.patients = this.sortBy('weightKg');
    } else {
      this.patients = this.sortBy('weightKg').reverse();
    }
    this.weightSort = !this.weightSort;
  }

  orderByHeight = () => {
    if (this.heightSort) {
      this.patients = this.sortBy('heightKg');
    } else {
      this.patients = this.sortBy('heightKg').reverse();
    }
    this.heightSort = !this.heightSort;
  }

  orderByBmi = () => {
    if (this.bmiSort) {
      this.patients = this.sortBy('bmi');
    } else {
      this.patients = this.sortBy('bmi').reverse();
    }
    this.bmiSort = !this.bmiSort;
  }

  isSubstring = (sub) => sub.includes(this.searchByName);

  showTableRow = (status) => this.filterByType === 'all' || this.filterByType === status;


  private sortBy = (type) => this.patients.sort((a, b) =>  parseFloat(a[type]) - parseFloat(b[type]));

}
