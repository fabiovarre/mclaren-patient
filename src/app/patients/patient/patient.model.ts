import { Activity } from '../../activities/activity.model';
export class Patient {

    id: number;
    name: string;
    gender: string;
    birthDate: string;
    heightCm: number;
    weightKg: number;
    bmi: number;
    activities: Activity[];
    showDetail: boolean = false;
    activityLevel: string = 'no data';

    constructor(patientDetails: any) {
        Object.assign(this, patientDetails);
    }
}
