import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Patient } from '../patients/patient/patient.model';
import { Activity } from '../activities/activity.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';


@Injectable()
export class PatientService {
    public baseUrl = '../';
    constructor(public http: Http) {
    }

    // getTenderType(id: string) {
    //     return this.http.get(fmt.fmt(this.tenderTypeUrl, id));
    // }
    public getPatients(): any {
        return this.http.get('./../../assets/mock-api-data/patients.json')
                        .map(this.getResponseAsPatientsList);

    }

    public getPatientsDetail = (id: number) => {
        return this.http.get(`./../../assets/mock-api-data/patients/${id}/summary.json`)
                        .map(this.getResponseAsActivitiesList);
    }

    getResponseAsPatientsList = (response: Response) => {
        const res = response.json().map(patient => new Patient(patient));
        return res || {};
    }

    getResponseAsActivitiesList = (response: Response) => {
        const res = response.json().map(activity => new Activity(activity));
        return res || {};
    }
}
