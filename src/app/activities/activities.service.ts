import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Activity } from '../activities/activity.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';


@Injectable()
export class ActivityService {
    public baseUrl = '../';
    constructor(public http: Http) {
    }

    // getTenderType(id: string) {
    //     return this.http.get(fmt.fmt(this.tenderTypeUrl, id));
    // }
    public getActivity = () => {
        return this.http.get('./../../assets/mock-api-data/definitions/activities.json')
                        .map(this.getResponseAsActivitiesList);

    }

    getResponseAsActivitiesList = (response: Response) => {
        const res = response.json().map(activity => new Activity(activity));
        return res || {};
    }
}
