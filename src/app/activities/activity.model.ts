export class Activity {
    activity: string;
    intensity: string;
    minutes: number;

    constructor(activityDetail: any) {
        Object.assign(this, activityDetail);
    }
}
