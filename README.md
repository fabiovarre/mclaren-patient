# MclarenPatient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.9.

I developed the application in angular2, using two main components and two services.

I used angular cli for scaffolding and application setup.
The main component is the Patient component container, which provides the data to the PatientTable child component that represents the data in the page.

I've implemented two filters and added the buttons to order each column in the table.

I have implemented two activities and patients needed to get the data and pass them to the application for the representation


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
I've write only few test because time was running, but i think that patient-component methods needs more test 

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
